#include "CaesarText.h"

#define DEFAULT_CAESAR_KEY 3


CaesarText::CaesarText(std::string text) : ShiftText(text, DEFAULT_CAESAR_KEY) { }

CaesarText::~CaesarText() { }


std::string CaesarText::encrypt()
{
	if (!_isEncrypted)
	{
		this->_isEncrypted = true;
		this->_text = this->encrypt(this->_text);
	}
	return this->_text;
}


std::string CaesarText::decrypt()
{
	if (this->_isEncrypted)
	{
		this->_isEncrypted = false;
		this->_text = this->encrypt(this->_text);
	}
	
	return this->_text;
}


std::string CaesarText::encrypt(std::string text)
{
	return ShiftText::encrypt(text, DEFAULT_CAESAR_KEY);
}

std::string CaesarText::decrypt(std::string text)
{
	return ShiftText::decrypt(text, DEFAULT_CAESAR_KEY);
}

std::ostream& operator<<(std::ostream os, CaesarText& c)
{
	os << "CaesarText" << std::endl;
	os << c.decrypt() << std::endl;

	return os;
}
