#ifndef PLAINTEXT_H
#define PLAINTEXT_H

#include <iostream>
#include <string>

class PlainText
{
public:
	static int _NumOfTexts;
	PlainText(std::string text);
	~PlainText();
	bool isEnc() const;
	std::string getText() const;
	friend std::ostream& operator<<(std::ostream& os, PlainText& p);

protected:
	std::string _text;
	bool _isEncrypted;
};
#endif // !PLAINTEXT_H