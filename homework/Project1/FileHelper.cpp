#include "FileHelper.h"

#define END_LINE '\n'

std::string FileHelper::readFileToString(std::string fileName)
{
    if (!FileHelper::isFileExist(fileName))
    {
        std::cerr << "[*] ERROR: file dosn't exist" << std::endl;
        _exit(1);
    }
    std::string file_text = "";
    std::ifstream myfile(fileName);
    char temp;
    
    if (myfile.is_open())
    {
        while ((temp = myfile.get()) != EOF)
        {
            file_text += temp;
        }
    }

    myfile.close();
    return file_text;
}

void FileHelper::writeWordsToFile(std::string inputFileName, std::string outputFileName)
{
    std::ifstream input_file(inputFileName);
    std::fstream output_file(outputFileName);
    std::string word;

    if ((!input_file.is_open()) || (!output_file.is_open()))
    {
        input_file.close();
        output_file.close();
        std::cerr << "[*] ERROR: file dosn't exist" << std::endl;
        _exit(1);
    }

    while (input_file >> word)
    {
        output_file << word << std::endl;
    }

    input_file.close();
    output_file.close();
}

void FileHelper::saveToNewFile(std::string text_to_save)
{
    std::string path = "";
    
    system("cls");
    std::cout << "Enter path to the new file (include .txt)" << std::endl;
    getchar(); //clean the buffer
    std::getline(std::cin, path);

    std::ofstream f(path);
    
    f << text_to_save;
    f.close();
}

bool FileHelper::isFileExist(std::string filePath)
{
    std::ifstream test(filePath);

    if (test.is_open())
    {
        test.close();
        return true;
    }
    return false;
}
