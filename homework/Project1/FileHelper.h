#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>


class FileHelper
{
public:
	static std::string readFileToString(std::string fileName);
	static void writeWordsToFile(std::string inputFileName, std::string outputFileName);
	static void saveToNewFile(std::string text_to_save);

protected:
	static bool isFileExist(std::string filePath);
};

#endif // !FILEHELPER_H