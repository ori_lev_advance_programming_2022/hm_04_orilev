#ifndef BONUSTEXT_H
#define BONUSTEXT_H

#include "PlainText.h"
#include <string>


class BonusText : public PlainText
{
public:
	BonusText(std::string txt);
	~BonusText();
	std::string decrypt(std::string decrypt_txt);
};
#endif // !BONUSTEXT_H