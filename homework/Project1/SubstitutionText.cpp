#include "SubstitutionText.h"
#include <iostream>
#include <map>

#define REQURIED_LINE_LENGHT 3

SubstitutionText::SubstitutionText(std::string text, std::string dictionaryFileName) : PlainText(text), _dictionaryFileName(dictionaryFileName)
{
	this->_text = encrypt();
	this->_isEncrypted = true;
}

SubstitutionText::~SubstitutionText()
{
}

std::string SubstitutionText::encrypt()
{
	if (!this->_isEncrypted)
	{
		this->_isEncrypted = true;
		this->_text = this->encrypt(this->_text, this->_dictionaryFileName);
	}
	return this->_text;
}

std::string SubstitutionText::decrypt()
{
	if (this->_isEncrypted)
	{
		this->_isEncrypted = false;
		this->_text = this->decrypt(this->_text, this->_dictionaryFileName);
	}
	return this->_text;
}



std::string SubstitutionText::encrypt(std::string text, std::string dictionaryFileName)
{
	std::map<char, char> hash_table = get_hash_table(dictionaryFileName);
	std::string to_return = "";

	for (std::size_t i = 0; i < text.length(); ++i)
	{
		if (((char)text[i] == ',') || ((char)text[i] == ' ') || ((char)text[i] == '.'))
		{
			to_return += text[i];
			continue;
		}
			
		to_return += hash_table[(char)text[i]];
	}
	return to_return;
}



std::string SubstitutionText::decrypt(std::string text, std::string dictionaryFileName)
{
	std::string to_return = "";
	std::map<char, char> hash_table = get_hash_table(dictionaryFileName);
	std::map<char, char> new_hash_table;

	// swap the hash table
	for (int i = (int)'a'; i <= (int)'z'; ++i)
		new_hash_table[hash_table[(char)i]] = (char)i;
		
	

	for (std::size_t i = 0; i < text.length(); ++i)
	{
		if (((char)text[i] == ',') || ((char)text[i] == ' ') || ((char)text[i] == '.'))
		{
			to_return += text[i];
			continue;
		}
			
		to_return += new_hash_table[(char)text[i]];
	}
	return to_return;
}



std::map<char, char> SubstitutionText::get_hash_table(std::string dictionaryFileName)
{
	std::map <char, char> swap_dict;
	std::string current_line;
	std::fstream dictionary;
	dictionary.open(dictionaryFileName);

	while (getline(dictionary, current_line))
	{
		if (current_line.length() != REQURIED_LINE_LENGHT)
			continue;
		swap_dict[current_line[0]] = current_line[2];
	}

	dictionary.close();
	return swap_dict;
}


std::ostream& operator<<(std::ostream os, SubstitutionText& s)
{
	os << "SubstitutionText" << std::endl;

	os << s.decrypt() << std::endl;
	return os;
}