#ifndef SHIFTTEXT_H
#define SHIFTTEXT_H

#include "PlainText.h"
#include <string>

class ShiftText : public PlainText
{
public:
	ShiftText(std::string text, int key);
	~ShiftText();
	std::string encrypt();
	std::string decrypt();
	static std::string encrypt(std::string text, int key);
	static std::string decrypt(std::string text, int key);
	friend std::ostream& operator<<(std::ostream os, ShiftText& s);
protected:
	int _key;
};
#endif // !SHIFTTEXT_H
