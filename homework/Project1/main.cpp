#include <ostream>
#include <string>
#include <fstream>

#include "CaesarText.h"
#include "FileHelper.h"
#include "ShiftText.h"
#include "SubstitutionText.h"

#define OPTION_ONE '1'
#define OPTION_TWO '2'
#define OPTION_THREE '3'
#define EXIT_CASE '4'

#define CAESAR '1'
#define SHIFT '2'
#define SUBSTITUTION '3'

#define DECRYPT 2
#define ENCRYPT 1

#define SAVE_TO_FILE 1
#define PRINT_TO_SCREEN 2
#define SUB_DICTIONARY_FILE_PATH "..\\..\\dictionary.csv"

void print_menu();
char get_option();
void handle_option_one();
char get_decrypt_method();
std::string get_decrypt_string();
bool check_string(std::string s);
void handle_file_encrypt_or_decrypt();
int decrypt_or_encrypt();
int get_key();
int print_or_save();

/// pointless function......
std::string get_caesar_text(const std::string txt);
std::string get_shift_text(const int k, const std::string txt);
std::string get_sub_text(const std::string txt, const std::string file_path);

int main()
{
	char option;

	do {
		option = get_option();

		switch (option)
		{
		case OPTION_ONE:
			handle_option_one();
			break;

		case OPTION_TWO:
			handle_file_encrypt_or_decrypt();
			break;

		case OPTION_THREE:
			std::cout << "Num of calls: " << PlainText::_NumOfTexts << std::endl;
			system("pause");
			break;

		case EXIT_CASE:
			std::cout << "\nBye Bye!" << std::endl;
			break;

		default:
			std::cerr << "Invalid number, try again." << std::endl;
			break;
		}

	} while (option != EXIT_CASE);


	return 0;
}

/*
* Function print the menu
* input: none
* output: none
*/
void print_menu(){
	std::cout << "Choose option:" << std::endl;
	std::cout << "1 - Decrypt string" << std::endl;
	std::cout << "2 - Decrypt file" << std::endl;
	std::cout << "3 - Show counter" << std::endl;
	std::cout << "4 - Exit" << std::endl;
}

/*
* function get option from the user
* input: none
* output: (char) the user choice
*/
char get_option()
{
	char option = ' ';
	bool ask_again = false;

	system("cls");

	print_menu();
	do {
		std::cin >> option;
		ask_again = false;

		if ((option > EXIT_CASE) || (option < OPTION_ONE))
		{
			system("cls");
			std::cerr << "[*] ERROR: Invalid number!" << std::endl;
			std::cerr << "[*] Try again.\n" << std::endl << std::endl;

			print_menu();
			ask_again = true;
		}
	} while (ask_again);

	return option;
}


char get_decrypt_method()
{
	char ch = ' ';
	bool again = false;

	do {

		std::cout << "\n\nChoose method:" << std::endl;
		std::cout << "[*] 1 - Caesar." << std::endl;
		std::cout << "[*] 2 - Shift." << std::endl;
		std::cout << "[*] 3 - Substitution." << std::endl << std::endl;

		std::cin >> ch;
		getchar();
		again = false;

		if (ch < OPTION_ONE || ch > OPTION_THREE)
		{
			system("cls");
			std::cout << "Invalid option, try again.\n" << std::endl;
			again = true;
		}
	} while (again);
	
	return ch;
}


bool check_string(std::string s)
{
	for (int i = 0; i < s.length(); ++i)
	{
		if (((s[i] != '.') && (s[i] != ' ')) && (s[i] != ',') && (!std::islower(s[i])))
			return false;
	}
	return true;
}


std::string get_decrypt_string()
{
	std::string s = "";
	std::cout << "Enter string to decrypt: ";
	
	std::getline(std::cin, s);

	return s;
}


void handle_option_one()
{
	
	std::string str_to_decrypt = "";
	char method = ' ';
	int key;
	
	method = get_decrypt_method();
	str_to_decrypt = get_decrypt_string();

	if (!check_string(str_to_decrypt))
	{
		std::cerr << "Invalid String!" << std::endl;
		system("pause");
		return;
	}

	switch (method)
	{
	case CAESAR:
		std::cout << "Decrypted string: " << get_caesar_text(str_to_decrypt) << std::endl;
		break;

	case SHIFT:
		std::cout << "Enter key: ";
		std::cin >> key;
		std::cout << "Decrypted string: " << get_shift_text(key, str_to_decrypt) << std::endl;
		break;

	case SUBSTITUTION:
		std::cout << "Decrypted string: " << get_sub_text(str_to_decrypt, SUB_DICTIONARY_FILE_PATH) << std::endl;
		break;

	default:
		break;
	}

	system("pause");
	system("cls");
}

int decrypt_or_encrypt()
{
	int i;
	std::cout << "\n\nChoose option:" << std::endl;
	std::cout << "1 - incrtypt"<< std::endl;
	std::cout << "2 - decrypt\n" << std::endl;
	do {
		std::cin >> i;

		if (i != ENCRYPT && i != DECRYPT)
			std::cerr << "Invalid option, try again." << std::endl;
		
	} while (i != ENCRYPT && i != DECRYPT);

	return i;
}


int get_key()
{
	int key;
	std::cout << "Enter key: ";
	std::cin >> key;
	return key;
}

int print_or_save()
{
	int i;
	std::cout << "\n\nChoose option:" << std::endl;
	std::cout << "1 - save" << std::endl;
	std::cout << "2 - print\n" << std::endl;
	do {
		std::cin >> i;

		if (i != SAVE_TO_FILE && i != PRINT_TO_SCREEN)
			std::cerr << "Invalid option, try again." << std::endl;

	} while (i != SAVE_TO_FILE && i != PRINT_TO_SCREEN);

	return i;
}


void handle_file_encrypt_or_decrypt()
{
	std::string file_name = "";
	char method = ' ';
	int dec_or_inc = 0;
	int what_to_do_with_the_file = 0;
	std::string file_text = "";

	std::cout << "Enter file path: ";
	getchar(); // clean the buffer
	std::getline(std::cin, file_name);

	method = get_decrypt_method();
	dec_or_inc = decrypt_or_encrypt();

	file_text = FileHelper::readFileToString(file_name);

	if (method == CAESAR && dec_or_inc == ENCRYPT)
		file_text = CaesarText::encrypt(file_text);

	else if (method == CAESAR && dec_or_inc == DECRYPT)
		file_text = CaesarText::decrypt(file_text);

	else if (method == SHIFT && dec_or_inc == ENCRYPT)
		file_text = ShiftText::encrypt(file_text, get_key());

	else if (method == SHIFT && dec_or_inc == DECRYPT)
		file_text = ShiftText::decrypt(file_text, get_key());

	else if (method == SUBSTITUTION && dec_or_inc == ENCRYPT)
		file_text = SubstitutionText::encrypt(file_text, SUB_DICTIONARY_FILE_PATH);

	else if(method == SUBSTITUTION && dec_or_inc == DECRYPT)
		file_text = SubstitutionText::decrypt(file_text, SUB_DICTIONARY_FILE_PATH);

	what_to_do_with_the_file = print_or_save();

	switch (what_to_do_with_the_file)
	{
	case PRINT_TO_SCREEN:
		system("cls");
		std::cout << file_text <<std::endl;
		system("pause");
		break;

	case SAVE_TO_FILE:
		FileHelper::saveToNewFile(file_text);
		break;
	}
}


std::string get_caesar_text(const std::string txt)
{
	CaesarText temp(txt);
	return temp.getText();
}

std::string get_shift_text(const int k, const std::string txt)
{
	ShiftText temp(txt, k);
	return temp.getText();
}

std::string get_sub_text(const std::string txt, const std::string file_path)
{
	SubstitutionText temp(txt, file_path);
	return temp.getText();
}
