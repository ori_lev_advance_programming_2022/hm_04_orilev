#ifndef CAESARTEXT_H
#define CAESARTEXT_H

#include "ShiftText.h"
#include <string>


class CaesarText : public ShiftText
{
public:
	CaesarText(std::string text);
	~CaesarText();
	std::string encrypt();
	std::string decrypt();
	static std::string encrypt(std::string text);
	static std::string decrypt(std::string text);
	
	friend std::ostream& operator<<(std::ostream os, CaesarText& c);
};
#endif // !CAESARTEXT_H