#include "PlainText.h"

int PlainText::_NumOfTexts = 0;

PlainText::PlainText(std::string text): 
	_text(text), 
	_isEncrypted(false)
{
	_NumOfTexts++;
}

PlainText::~PlainText() { }

bool PlainText::isEnc() const
{
	return this->_isEncrypted;
}

std::string PlainText::getText() const
{
	return this->_text;
}

std::ostream& operator<<(std::ostream& os, PlainText& p)
{
	os << "Plain Text" << std::endl;

	os << p._text << std::endl;

	return os;
}
