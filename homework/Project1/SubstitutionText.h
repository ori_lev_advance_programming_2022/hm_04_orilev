#ifndef SUBSTITUTIONTEXT_H
#define SUBSTITUTIONTEXT_H

#include "PlainText.h"
#include <string>
#include <fstream>
#include <iostream>
#include <map>





class SubstitutionText : public PlainText
{
public:
	SubstitutionText(std::string text, std::string dictionaryFileName);
	~SubstitutionText();
	std::string encrypt();
	std::string decrypt();
	static std::string encrypt(std::string text, std::string dictionaryFileName);
	static std::string decrypt(std::string text, std::string dictionaryFileName);

	friend std::ostream& operator<<(std::ostream os, SubstitutionText& s);

protected:
	static std::map<char, char> get_hash_table(std::string dictionaryFileName);


private:
	
	std::string _dictionaryFileName;
};
#endif // !SUBSTITUTIONTEXT_H