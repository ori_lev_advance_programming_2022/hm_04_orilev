#include "ShiftText.h"

#define ABC_LEN 26
#define ASCII_A (int)'a'
#define ASCII_Z (int)'z'


ShiftText::ShiftText(std::string text, int key) : PlainText(text), _key(key)
{
	this->_text = this->encrypt();
	this->_isEncrypted = true;
}


ShiftText::~ShiftText()
{ }


std::string ShiftText::encrypt()
{
	if (this->_isEncrypted == false)
	{
		this->_isEncrypted = true;
		this->_text = this->encrypt(this->_text, this->_key);
	}
	return this->_text;
}

std::string ShiftText::decrypt()
{
	if (this->_isEncrypted)
	{
		this->_isEncrypted = false;
		this->_text = this->decrypt(this->_text, this->_key);
	}
	
	return this->_text;
}

std::string ShiftText::encrypt(std::string text, int key)
{
	std::string temp = "";
	key = key % ABC_LEN;

	for (std::size_t i = 0; i < text.length(); ++i)
	{
		if (((char)text[i] == ',') || ((char)text[i] == ' ') || ((char)text[i] == '.'))
		{
			temp += text[i];
			continue;
		}
		temp += (char)(ASCII_A + (text[i] - ASCII_A + key + ABC_LEN) % ABC_LEN);
	}
	return temp;
}

std::string ShiftText::decrypt(std::string text, int key)
{
	std::string temp = "";
	key = key % ABC_LEN;

	for (std::size_t i = 0; i < text.length(); ++i)
	{
		if (((char)text[i] == ',') || ((char)text[i] == ' ') || ((char)text[i] == '.'))
		{
			temp += text[i];
			continue;
		}
		temp += (char)(ASCII_A + (text[i] - ASCII_A - key + ABC_LEN) % ABC_LEN);
	}
	return temp;
	
}

std::ostream& operator<<(std::ostream os, ShiftText& s)
{
	os << "ShiftText" << std::endl;
	os << s.decrypt() << std::endl;

	return os;
}
