#include "ShiftText.h"
#include "SubstitutionText.h"
#include "CaesarText.h"
#include "FileHelper.h"
#include <iostream>
#include <string>


#define SHIFTING_KEY 10
#define SUB_DICTIONARY_FILE_NAME "..\\..\\dictionary.csv"


using std::cout;
using std::endl;


int test()
{

	std::string to_check = "dfskhfdsjkhfdajhzjkrwiuo";
	bool is_equal = ShiftText::decrypt(ShiftText::encrypt(to_check, 5), 5) == to_check;
	cout << "Shift: " << is_equal << endl;

	is_equal = to_check == CaesarText::decrypt(CaesarText::encrypt(to_check));
	cout << "Caesar: " << is_equal << endl;

	is_equal = SubstitutionText::decrypt(SubstitutionText::encrypt(to_check, SUB_DICTIONARY_FILE_NAME), SUB_DICTIONARY_FILE_NAME) == to_check;
	cout << "SUB: " << is_equal << endl;
	return 0;
}
